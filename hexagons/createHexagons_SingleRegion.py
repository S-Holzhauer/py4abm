'''
Created on 23 Jul 2014

@author: Sascha Holzhauer

Uses raster data as input to determine area to cover by hexagons.

Instructions:
1. Save the script with another filename
2. Adapt parameters
    a) outputDir and qgisPath
    b) regionName
    c) hexWidth1 and hexWidth2
    d) optional: set switches
    e) optional: adapt filename patterns
3. Call the python script with switch doRasterConversion = 1 (others 0)
4. Merge the raster manually in QGIS (this script currently does not support merging):
    a) Raster > Raster Calculator
    b) Select the raster input file from "Raster bands" (double click)
    c) Add "> -9"
    d) Select GeoTIFF as output format
    e) Make sure "Add result to project" is ticked
    f) Specify an output file (eg. REGION_raster_merged.asc)
    g) OK
5. Call the python script with switch doRasterConversion = 0 (others 1 as preferred)

'''

import re

from qgis.core import QgsApplication, QgsVectorLayer
from qgis.analysis import QgsGeometryAnalyzer 
from util.selectHexagons import writeCoveredHexagons
from util.createHexagons_mmqgis import mmqgis_grid

outputDir = "C:/Data/LURG/Projects/Volante/Data/shp/hexagon/%REGION%/"
qgisPath = "C:/Program Files/OSGeo4W64/apps/qgis"

regionName = "Iberia"

hexWidth1 = 75
hexWidth2 = 10

# Switches:
doRasterConversion = 0
doRasterMerge = 0

doBuffering = 1
doHexagonShapefile = 1
doSelectHexagons = 1

# Filename patterns:
rasterRaw = "Baseline-%REGION%-0-0-Agent-SerialID-1.asc"
rasterConverted = "Baseline-%REGION%-0-0-Agent-SerialID-1_converted.asc"

rasterConvertedMerged = "Baseline-%REGION%-0-0-Agent-SerialID-1_converted_merged.tif"

shp_rasterPolygonised = "%REGION%_raster_merged_polygonised_crs.shp"
shp_rasterPolygonisedBuffered = "%REGION%_raster_merged_polygonised_crs_buffered.shp"
shp_hex1 = "hexagons_%REGION%_%HEXWIDTH1%_1st_raw.shp"
shp_hex2 = "hexagons_%REGION%_%HEXWIDTH2%_2nd_raw.shp"

shp_hex1_final = "hexagons_%REGION%_%HEXWIDTH1%_1st.shp"
shp_hex2_final = "hexagons_%REGION%_%HEXWIDTH2%_2nd.shp"



rep = {
'%REGION%': regionName,
'%HEXWIDTH1%':str(hexWidth1),
'%HEXWIDTH2%':str(hexWidth2)
}
pattern = re.compile('|'.join(rep.keys()))

shp_rasterPolygonisedBuffered_real = pattern.sub(lambda x: rep[x.group()], outputDir + shp_rasterPolygonisedBuffered)
shp_rasterPolygonised_real = pattern.sub(lambda x: rep[x.group()], outputDir + shp_rasterPolygonised)

shp_hex1_real = pattern.sub(lambda x: rep[x.group()], '' + outputDir + shp_hex1)
shp_hex2_real = pattern.sub(lambda x: rep[x.group()], '' + outputDir + shp_hex2)

targetLayerPath1 = pattern.sub(lambda x: rep[x.group()], outputDir + shp_hex1_final)
targetLayerPath2 = pattern.sub(lambda x: rep[x.group()], outputDir + shp_hex2_final)

# supply path to where is your qgis installed
QgsApplication.setPrefixPath(qgisPath, True)

# load providers
QgsApplication.initQgis()
 
# convert raster file
if doRasterConversion:
    print("Raster file conversion...")
    inputRasterFile =  pattern.sub(lambda x: rep[x.group()], outputDir + rasterRaw)
    outputRasterFile = pattern.sub(lambda x: rep[x.group()], outputDir + rasterConverted)
    
    infile = open(inputRasterFile, 'r')
    inputString =  infile.readline()
    
    oFile = open(outputRasterFile,'wb')
    while inputString != "":
        oFile.write(inputString.replace("null", "-9"))
        inputString = infile.readline()
    infile.close()
    print("Raster file " + inputRasterFile + " converted to " + outputRasterFile)
    
# merge raster layer
if doRasterMerge:
    rasterRawReal = pattern.sub(lambda x: rep[x.group()], outputDir + rasterConverted)
    rasterMergedReal = pattern.sub(lambda x: rep[x.group()], outputDir + rasterConvertedMerged)
    #fileInfo = QFileInfo(rasterRawReal)
    #baseName = fileInfo.baseName()
    #rlayer = QgsRasterLayer(rasterRawReal, baseName)
    
    # uses http://code.google.com/p/gdal-calculations/ but does not work
    #gdal.UseExceptions()
# 
#     ds1=Dataset(rasterRawReal)
#     band = ds1[0]
#     bandf = Float32(band)
#     b= bandf * 2
#     ds2=ds1[0]*2
#     ds2.save(rasterMergedReal)

#     print("Raster file " + rasterRawReal + " merged to " + rasterMergedReal)
    
# add buffer
if doBuffering:
    layer = QgsVectorLayer(shp_rasterPolygonised_real, shp_rasterPolygonised, "ogr")
    
    QgsGeometryAnalyzer().buffer(layer, shp_rasterPolygonisedBuffered_real, 3, False, False, -1)
    print("Buffered raster vector file " + shp_rasterPolygonisedBuffered_real + " created")
    
# load raster layer
if doHexagonShapefile:
    print "Start creation of hexagon shapefiles..."
    # get extends of polygonised raster shapefile
    layer = QgsVectorLayer(shp_rasterPolygonisedBuffered_real, shp_rasterPolygonisedBuffered, "ogr")
    if not layer.isValid():
        print "Layer (" + shp_rasterPolygonisedBuffered_real + ") failed to load!"
    else:
        xMax = layer.extent().xMaximum()
        yMax = layer.extent().yMaximum()
        
        xMin = layer.extent().xMinimum()
        yMin = layer.extent().yMinimum()
        
        # create 1st layer hexagon:
       
        mmqgis_grid(shp_hex1_real, 1, hexWidth1, xMax-xMin+(2*hexWidth1), yMax-yMin+(2*hexWidth1), xMin-hexWidth1, yMin-hexWidth1)
        print("1st Hexagon file " + shp_hex1_real + " created")
        
        # create 2nd layer hexagon:
        mmqgis_grid(shp_hex2_real, 1, hexWidth2, xMax-xMin+(2*hexWidth2), yMax-yMin+(2*hexWidth2), xMin-hexWidth2, yMin-hexWidth2)
        print("2nd Hexagon file " + shp_hex2_real + " created")
 
if doSelectHexagons:
    print("Start output selection of hexagons...")
    layerToSelect1 = QgsVectorLayer(shp_hex1_real, shp_hex1, "ogr")
    if not layerToSelect1.isValid():
        print "Layer (" + shp_hex1_real + ") failed to load!"
    else:
        layerToSelect2 = QgsVectorLayer(shp_hex2_real, shp_hex2, "ogr")
        if not layerToSelect2.isValid():
            print "Layer (" + shp_hex2_real + ") failed to load!"
        else:
            print("Start output selection of hexagons...")
            bufferLayer = QgsVectorLayer(shp_rasterPolygonisedBuffered_real, shp_rasterPolygonisedBuffered, "ogr")
            if not bufferLayer.isValid():
                print "Layer (" + shp_rasterPolygonisedBuffered_real + ") failed to load!"
            else:
                writeCoveredHexagons(shp_hex1_real, shp_rasterPolygonisedBuffered_real, targetLayerPath1)
                print("Output selection 1st Hexagon file " + targetLayerPath1)
                writeCoveredHexagons(shp_hex2_real, shp_rasterPolygonisedBuffered_real, targetLayerPath2)
                print("Output selection 2nd Hexagon file " + targetLayerPath2)
    
QgsApplication.exitQgis()