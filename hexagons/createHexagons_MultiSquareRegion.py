'''
Created on 23 Jul 2014

@author: Sascha Holzhauer

Uses world width and regionalisation pattern to determine area to cover by hexagons  
(no raster data as input required).

Instructions:
1. Save the script with another filename
2. Adapt parameters
    a) outputDir and outputDirTmp, qgisPath
    b) worldName
    c) worldWidth
    d) regionalisations
    e) hexWidth1 and hexWidth2
    f) optional: set switches
    g) optional: adapt filename patterns
3. Call the python script

'''

import os, shutil
import glob
import math

import qgis.core
import re

from qgis.analysis import QgsGeometryAnalyzer 
from util.selectHexagons import writeCoveredHexagons
from util.createHexagons_mmqgis import mmqgis_grid
from util.createGridPolygons import createGridPolygons

outputDir    = "C:/Data/LURG/workspace/CRAFTY_ConsVis-ToyWorld/data/worlds/%WORLD%/regionalisations/shp/hexagon/%REGIONALISATION%/%REGION%/"
outputDirTmp = "C:/Data/LURG/workspace/CRAFTY_ConsVis-ToyWorld/data/worlds/%WORLD%/regionalisations/shp/hexagon/%REGIONALISATION%/tmp/"
qgisPath = "C:/Program Files/OSGeo4W64/apps/qgis"

worldName = "ToyWorldA"

worldWidth = 200
regionalisations = [1, 4, 16]

hexWidth1 = 40
hexWidth2 = 15

xMinimum = 0
yMinimum = 0


# Switches
doGridCreation = 1
doGridDevitionCreation = 0
doBuffering = 0
doHexagonShapefile = 1
doSelectHexagons = 1
doClean = 1

debug = 0


# Filename patterns
shp_grid =  "%WORLD%_%REGIONALISATION%_devitionGrid_crs.shp"
shp_grid_region =  "%WORLD%_%REGIONALISATION%_%REGION%_grid_crs.shp"

shp_rasterPolygonised = "%WORLD%_%REGIONALISATION%_%REGION%_raster_merged_polygonised_crs.shp"
shp_rasterPolygonisedBuffered = "%WORLD%_%REGIONALISATION%_%REGION%_raster_merged_polygonised_crs_buffered.shp"

shp_hex1_raw = "hexagons_%WORLD%_%REGIONALISATION%_%REGION%_%HEXWIDTH1%_1st_raw.shp"
shp_hex2_raw = "hexagons_%WORLD%_%REGIONALISATION%_%REGION%_%HEXWIDTH1%_2nd_raw.shp"

shp_hex1_final = "hexagons_%WORLD%_%REGIONALISATION%_%REGION%_%HEXWIDTH1%_1st.shp"
shp_hex2_final = "hexagons_%WORLD%_%REGIONALISATION%_%REGION%_%HEXWIDTH2%_2nd.shp"


# supply path to where is your qgis installed
qgis.core.QgsApplication.setPrefixPath(qgisPath, True)

# load providers
qgis.core.QgsApplication.initQgis()
crs = qgis.core.QgsCoordinateReferenceSystem()
crs.createFromString("EPSG:7416")


for regionalisation in regionalisations:
    print("Handle regionalisation " + str(regionalisation) + "...")
    
    rep = {
        '%WORLD%': worldName,
        '%REGIONALISATION%': str(regionalisation),
        '%HEXWIDTH1%':str(hexWidth1),
        '%HEXWIDTH2%':str(hexWidth2)
        }
    pattern = re.compile('|'.join(rep.keys()))
    
    outputDirTmp_real = pattern.sub(lambda x: rep[x.group()], '' + outputDirTmp)
   
    if os.path.exists(outputDirTmp_real):
        shutil.rmtree(outputDirTmp_real, ignore_errors=True)
    os.makedirs(outputDirTmp_real)
    
    
    regionsPerSide = math.sqrt(regionalisation)
    
    
    
    if doGridDevitionCreation:
        shp_grid_real = pattern.sub(lambda x: rep[x.group()], '' + outputDirTmp + shp_grid)

        qgis.core.QgsApplication.initQgis()
        createGridPolygons(shp_grid_real, crs, xMinimum, xMinimum + worldWidth, yMinimum, 
                           yMinimum + worldWidth, worldWidth/regionalisation, worldWidth/regionalisation)
        qgis.core.QgsApplication.exitQgis()
    
    for r in range(1,regionalisation+1):
        
        regionName = chr(r - 1 + ord('A'))
        print("Handle regionName " + regionName + "...") 
    
        rep = {
        '%WORLD%': worldName,
        '%REGION%': regionName,
        '%REGIONALISATION%': str(regionalisation),
        '%HEXWIDTH1%':str(hexWidth1),
        '%HEXWIDTH2%':str(hexWidth2)
        }
        pattern = re.compile('|'.join(rep.keys()))

        outputDir_real = pattern.sub(lambda x: rep[x.group()], '' + outputDir)
        if os.path.exists(outputDir_real):
            shutil.rmtree(outputDir_real, ignore_errors=True)
        os.makedirs(outputDir_real)
    
        if doGridCreation:
            
            shp_grid_region_real = pattern.sub(lambda x: rep[x.group()], '' + outputDirTmp + shp_grid_region)

            # row-wise from the bottom!
            xrow = math.ceil(r / regionsPerSide) - 1 
            ycol = (r-1) % regionsPerSide
            regionWidth = worldWidth / regionsPerSide
            
            xMinimum = xrow * regionWidth
            yMinimum = ycol * regionWidth 

            createGridPolygons(shp_grid_region_real, crs, xMinimum, xMinimum + regionWidth, yMinimum, 
                               yMinimum + regionWidth, regionWidth + 0.0001, regionWidth + 0.0001)
           
       
        shp_rasterPolygonisedBuffered_real = pattern.sub(lambda x: rep[x.group()], outputDir + shp_rasterPolygonisedBuffered)
        shp_rasterPolygonised_real = pattern.sub(lambda x: rep[x.group()], outputDir + shp_rasterPolygonised)
        
        shp_hex1_raw_real = pattern.sub(lambda x: rep[x.group()], '' + outputDir + shp_hex1_raw)
        shp_hex2_raw_real = pattern.sub(lambda x: rep[x.group()], '' + outputDir + shp_hex2_raw)
        
        shp_hex1_final_real = pattern.sub(lambda x: rep[x.group()], outputDir + shp_hex1_final)
        shp_hex2_final_real = pattern.sub(lambda x: rep[x.group()], outputDir + shp_hex2_final)

        
        # add buffer
        if doBuffering:
            layer = qgis.core.QgsVectorLayer(shp_grid_region_real, shp_grid_region, "ogr")
            
            QgsGeometryAnalyzer().buffer(layer, shp_rasterPolygonisedBuffered_real, 3, False, False, -1)
            if debug:
                print("Buffered raster vector file " + shp_rasterPolygonisedBuffered_real + " created")
            
        # load raster layer
        if doHexagonShapefile:
            
            if not doBuffering:
                shp_rasterPolygonisedBuffered_real = shp_grid_region_real
                if debug:
                    print(shp_rasterPolygonisedBuffered_real)
            
            if debug:
                print "Start creation of hexagon shapefiles..."
            # get extends of polygonised raster shapefile
            layer = qgis.core.QgsVectorLayer(shp_rasterPolygonisedBuffered_real, shp_rasterPolygonisedBuffered, "ogr")
            if not layer.isValid():
                print "Layer (" + shp_rasterPolygonisedBuffered_real + ") failed to load!"
            else:
                xMax = layer.extent().xMaximum()
                yMax = layer.extent().yMaximum()
                
                xMin = layer.extent().xMinimum()
                yMin = layer.extent().yMinimum()
                
                # create 1st layer hexagon:
               
                mmqgis_grid(shp_hex1_raw_real, 1, hexWidth1, xMax-xMin+(2*hexWidth1), yMax-yMin+(2*hexWidth1), xMin-hexWidth1, yMin-hexWidth1)
                if debug:
                    print("1st Hexagon file " + shp_hex1_raw_real + " created")
                
                # create 2nd layer hexagon:
                mmqgis_grid(shp_hex2_raw_real, 1, hexWidth2, xMax-xMin+(2*hexWidth2), yMax-yMin+(2*hexWidth2), xMin-hexWidth2, yMin-hexWidth2)
                if debug:
                    print("2nd Hexagon file " + shp_hex2_raw_real + " created")
                del layer
                
        if doSelectHexagons:
            if debug:
                print("Start output selection of hexagons...")
            layerToSelect1 = qgis.core.QgsVectorLayer(shp_hex1_raw_real, shp_hex1_raw, "ogr")
            if not layerToSelect1.isValid():
                print "Layer (" + shp_hex1_raw_real + ") failed to load!"
            else:
                layerToSelect2 = qgis.core.QgsVectorLayer(shp_hex2_raw_real, shp_hex2_raw, "ogr")
                if not layerToSelect2.isValid():
                    print "Layer (" + shp_hex2_raw_real + ") failed to load!"
                else:
                    if debug:
                        print("Start output selection of hexagons...")
                    bufferLayer = qgis.core.QgsVectorLayer(shp_rasterPolygonisedBuffered_real, shp_rasterPolygonisedBuffered, "ogr")
                    if not bufferLayer.isValid():
                        print "Layer (" + shp_rasterPolygonisedBuffered_real + ") failed to load!"
                    else:
                        writeCoveredHexagons(shp_hex1_raw_real, shp_rasterPolygonisedBuffered_real, shp_hex1_final_real)
                        if debug:
                            print("Output selection 1st Hexagon file " + shp_hex1_final_real)
                        writeCoveredHexagons(shp_hex2_raw_real, shp_rasterPolygonisedBuffered_real, shp_hex2_final_real)
                        if debug:
                            print("Output selection 2nd Hexagon file " + shp_hex2_final_real + ".")
                        del layerToSelect1
                        del layerToSelect2
                        del bufferLayer

        if doClean:
            if doSelectHexagons:
                

                for fl in glob.glob(shp_hex1_raw_real[:-3] + "*"):
                    os.remove(fl)
                
                for fl in glob.glob(shp_hex2_raw_real[:-3] + "*"):
                    os.remove(fl)

    if doClean:        
        shutil.rmtree(outputDirTmp_real)

qgis.core.QgsApplication.exitQgis()
