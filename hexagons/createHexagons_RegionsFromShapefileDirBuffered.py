'''
Created on 23 Jul 2014

@author: Sascha Holzhauer

Uses single shapefiles per region in a folder (created from raster data and split in QGIS)
to determine area to cover by hexagons.

Instructions:
1. Save the script with another filename
2. Adapt parameters
    a) outputDir and qgisPath
    b) regionShapeDir source directory of boundary shapefiles
    c) hexWidth1 and hexWidth2
    d) regionalisation name for regionalisation
    e) optional: set switches
    f) optional: adapt filename patterns
3. Create a vector layer from "Final countries raster" which contains all regions
    (QGIS: Raster > Conversion > Polygonize).
4. Create one shapefile per region (i.e. value in the "Final countries raster"):
    QGIS: Vector > Data Management Tools > Split vector layer
    
    The R function craftyr::convert_csv2shapefile can be applied to generate polygon
    shapefiles from CSV coordinates (for multiple files at once), e.g. from capital files.
5. Call this python script

@data 22/11/2016 adapted to QGIS Essen

'''

import re
import sys
import glob, os

from shutil import rmtree
from PyQt4.QtGui import *
from qgis.core import QgsApplication, QgsVectorLayer
from qgis.analysis import QgsGeometryAnalyzer 
from util.selectHexagons import writeCoveredHexagons
from util.createHexagons_mmqgis import mmqgis_grid

outputDir = "C:/Data/LURG/workspace/CRAFTY_ConsVis-ToyWorld/data/worlds/EU28/regionalisations/shp/hexagon/%REGIONALISATION%/%REGION%/"
qgisPath = "C:/Program Files/OSGeo4W64/apps/qgis"

regionShapeDir = "C:/Data/LURG/Projects/Volante/ConsolidatedVisions/shp/SingleRegions"

regionalisation = "28"

#crs_string = "EPSG:7416" #WGS 84
crs_string = "EPSG:32632" #WGS 84 / UTM zone 32N

hexWidth1 = 75
hexWidth2 = 10


doBuffering = 0
doHexagonShapefile = 0
doSelectHexagons = 0
deleteRawFiles = 1
# Filename patterns:

shp_buffered = "%REGION%_buffered.shp"
shp_hex1 = "hexagons_%REGION%_%HEXWIDTH1%_1st_raw.shp"
shp_hex2 = "hexagons_%REGION%_%HEXWIDTH2%_2nd_raw.shp"

shp_hex1_final = "hexagons_%REGION%_%HEXWIDTH1%_1st.shp"
shp_hex2_final = "hexagons_%REGION%_%HEXWIDTH2%_2nd.shp"

print("Init...")

# supply path to where is your qgis installed
app = QApplication(sys.argv)
QgsApplication.setPrefixPath(qgisPath, True)

# load providers
QgsApplication.initQgis()
 
print("Process...")
for regionShapefile in glob.iglob(os.path.join(regionShapeDir, "*.shp")):
    print("Process " + regionShapefile + "...")
    
    shapeTitle, ext = os.path.splitext(os.path.basename(regionShapefile))
    # regionNumber = shapeTitle
    regionNumber = re.findall(r'[0-9]+', shapeTitle)[0]
    
    rep = {
    '%REGION%': str(regionNumber),
    '%REGIONALISATION%': regionalisation,
    '%HEXWIDTH1%':str(hexWidth1),
    '%HEXWIDTH2%':str(hexWidth2)
    }
    
    pattern = re.compile('|'.join(rep.keys()))

    shp_rasterPolygonisedBuffered_real = pattern.sub(lambda x: rep[x.group()], outputDir + shp_buffered)

    shp_hex1_real = pattern.sub(lambda x: rep[x.group()], '' + outputDir + shp_hex1)
    shp_hex2_real = pattern.sub(lambda x: rep[x.group()], '' + outputDir + shp_hex2)
    
    targetLayerPath1 = pattern.sub(lambda x: rep[x.group()], outputDir + shp_hex1_final)
    targetLayerPath2 = pattern.sub(lambda x: rep[x.group()], outputDir + shp_hex2_final)

    
    outdir = pattern.sub(lambda x: rep[x.group()], outputDir)
    
    if (doBuffering and doHexagonShapefile and doSelectHexagons):
        if os.path.exists(outdir):
            rmtree(outdir, ignore_errors=True)    
        os.makedirs(outdir)
    
    # add buffer
    if doBuffering:
        print("Load vector file " + regionShapefile + "..." + shapeTitle + "--" + ext)
        layer = QgsVectorLayer(regionShapefile, shapeTitle + ext, "ogr")
       
        print("Buffered raster vector file " + shp_rasterPolygonisedBuffered_real + " created") 
        QgsGeometryAnalyzer().buffer(layer, shp_rasterPolygonisedBuffered_real, 3, False, False, -1)
    
    # load raster layer
    if doHexagonShapefile:
        print "Start creation of hexagon shapefiles..."
        
        # get extends of polygonised raster shapefile
        layer = QgsVectorLayer(shp_rasterPolygonisedBuffered_real, shp_buffered, "ogr")
        if not layer.isValid():
            print "Layer (" + shp_rasterPolygonisedBuffered_real + ") failed to load!"
        else:
            xMax = layer.extent().xMaximum()
            yMax = layer.extent().yMaximum()
            
            xMin = layer.extent().xMinimum()
            yMin = layer.extent().yMinimum()
            
            # create 1st layer hexagon:
            mmqgis_grid(shp_hex1_real, 1, hexWidth1, xMax-xMin+(2*hexWidth1), yMax-yMin+(2*hexWidth1), xMin-hexWidth1, yMin-hexWidth1, crs_string = crs_string)
            print("1st Hexagon file " + shp_hex1_real + " created")
            
            # create 2nd layer hexagon:
            mmqgis_grid(shp_hex2_real, 1, hexWidth2, xMax-xMin+(2*hexWidth2), yMax-yMin+(2*hexWidth2), xMin-hexWidth2, yMin-hexWidth2, crs_string = crs_string)
            print("2nd Hexagon file " + shp_hex2_real + " created")
            
            del layer
     
    if doSelectHexagons:
        print("Start output selection of 1st layer hexagons...")
        layerToSelect1 = QgsVectorLayer(shp_hex1_real, shp_hex1, "ogr")
        if not layerToSelect1.isValid():
            print "Layer (" + shp_hex1_real + ") failed to load!"
        else:
            layerToSelect2 = QgsVectorLayer(shp_hex2_real, shp_hex2, "ogr")
            if not layerToSelect2.isValid():
                print "Layer (" + shp_hex2_real + ") failed to load!"
            else:
                print("Start output selection of 2nd layer hexagons...")
                
                bufferLayer = QgsVectorLayer(shp_rasterPolygonisedBuffered_real, shapeTitle, "ogr")
                if not bufferLayer.isValid():
                    print "Layer (" + shp_rasterPolygonisedBuffered_real + ") failed to load!"
                else:
                    writeCoveredHexagons(shp_hex1_real, shp_rasterPolygonisedBuffered_real, targetLayerPath1)
                    print("Output selection 1st Hexagon file " + targetLayerPath1)
                    writeCoveredHexagons(shp_hex2_real, shp_rasterPolygonisedBuffered_real, targetLayerPath2)
                    print("Output selection 2nd Hexagon file " + targetLayerPath2)
                del bufferLayer
                del layerToSelect1
                del layerToSelect2
                    
    if deleteRawFiles:
        print("Delete buffered and raw files from " + outdir)
        
        for fl in glob.glob(shp_rasterPolygonisedBuffered_real[0:-4] + ".*"):
            os.remove(fl)
        for fl in glob.glob(shp_hex1_real[0:-4] + ".*"):
            os.remove(fl)
        for fl in glob.glob(shp_hex2_real[0:-4] + ".*"):
            os.remove(fl)
        
        
QgsApplication.exitQgis()
