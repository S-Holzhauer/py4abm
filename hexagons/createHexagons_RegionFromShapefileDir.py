'''
Created on 16 Jul 2015

@author: Sascha Holzhauer

Uses a single shapefiles in a folder without buffering
to determine area to cover by hexagons. Does not consider
regionalisation.

Instructions:
1. Save the script with another filename
2. Adapt parameters
    a) outputDir and qgisPath
    b) regionShapeDir source directory of boundary shapefiles
    c) hexWidth1 and hexWidth2
    d) optional: set switches
    e) optional: adapt filename patterns
3. Create one shapefile per region
4. Call this python script

'''

import re
import glob, os

from shutil import rmtree
from qgis.core import QgsApplication, QgsVectorLayer
from qgis.analysis import QgsGeometryAnalyzer 
from util.selectHexagons import writeCoveredHexagons
from util.createHexagons_mmqgis import mmqgis_grid

outputDir = "C:/Data/Dis/workspace/KUBUS_SNA-01/config/shapes/hexagons/3500/"
qgisPath = "C:/Program Files/OSGeo4W64/apps/qgis"

regionShapeDir = "C:/Data/Dis/workspace/KUBUS_SNA-01/data/areaShapes/area3500/"



hexWidth1 = 1500
hexWidth2 = 200

doHexagonShapefile = 1
doSelectHexagons = 1
deleteRawFiles = 1
# Filename patterns:

regionalisation = "500"

shp_buffered = "%REGION%_buffered.shp"
shp_hex1 = "hexagons_%REGION%_%HEXWIDTH1%_1st_raw.shp"
shp_hex2 = "hexagons_%REGION%_%HEXWIDTH2%_2nd_raw.shp"

shp_hex1_final = "hexagons_%REGION%_%HEXWIDTH1%_1st.shp"
shp_hex2_final = "hexagons_%REGION%_%HEXWIDTH2%_2nd.shp"


# supply path to where is your qgis installed
QgsApplication.setPrefixPath(qgisPath, True)

# load providers
QgsApplication.initQgis()
 

for regionShapefile in glob.iglob(os.path.join(regionShapeDir, "*.shp")):
    
    shapeTitle, ext = os.path.splitext(os.path.basename(regionShapefile))
    regionNumber = re.findall(r'[0-9]+', shapeTitle)[0]
    
    rep = {
    '%REGION%': str(regionNumber),
    '%REGIONALISATION%': regionalisation,
    '%HEXWIDTH1%':str(hexWidth1),
    '%HEXWIDTH2%':str(hexWidth2)
    }
    
    pattern = re.compile('|'.join(rep.keys()))

    shp_rasterPolygonisedBuffered_real = pattern.sub(lambda x: rep[x.group()], outputDir + shp_buffered)

    shp_hex1_real = pattern.sub(lambda x: rep[x.group()], '' + outputDir + shp_hex1)
    shp_hex2_real = pattern.sub(lambda x: rep[x.group()], '' + outputDir + shp_hex2)
    
    targetLayerPath1 = pattern.sub(lambda x: rep[x.group()], outputDir + shp_hex1_final)
    targetLayerPath2 = pattern.sub(lambda x: rep[x.group()], outputDir + shp_hex2_final)

    
    outdir = pattern.sub(lambda x: rep[x.group()], outputDir)
    
    if (doHexagonShapefile and doSelectHexagons):
        if os.path.exists(outdir):
            rmtree(outdir, ignore_errors=True)    
        os.makedirs(outdir)
    
    # load raster layer
    if doHexagonShapefile:
        print "Start creation of hexagon shapefiles..."
        
        print("Load vector file " + regionShapefile + "..." + shapeTitle + "--" + ext)
        layer = QgsVectorLayer(regionShapefile, shapeTitle + ext, "ogr")
        
        if not layer.isValid():
            print "Layer (" + regionShapefile + ") failed to load!"
        else:
            xMax = layer.extent().xMaximum()
            yMax = layer.extent().yMaximum()
            
            xMin = layer.extent().xMinimum()
            yMin = layer.extent().yMinimum()
            
            # create 1st layer hexagon:
            mmqgis_grid(shp_hex1_real, 1, hexWidth1, xMax-xMin+(2*hexWidth1), yMax-yMin+(2*hexWidth1), xMin-hexWidth1, yMin-hexWidth1)
            print("1st Hexagon file " + shp_hex1_real + " created")
            
            # create 2nd layer hexagon:
            mmqgis_grid(shp_hex2_real, 1, hexWidth2, xMax-xMin+(2*hexWidth2), yMax-yMin+(2*hexWidth2), xMin-hexWidth2, yMin-hexWidth2)
            print("2nd Hexagon file " + shp_hex2_real + " created")
     
    if doSelectHexagons:
        print("Start output selection of 1st layer hexagons...")
        layerToSelect1 = QgsVectorLayer(shp_hex1_real, shp_hex1, "ogr")
        if not layerToSelect1.isValid():
            print "Layer (" + shp_hex1_real + ") failed to load!"
        else:
            layerToSelect2 = QgsVectorLayer(shp_hex2_real, shp_hex2, "ogr")
            if not layerToSelect2.isValid():
                print "Layer (" + shp_hex2_real + ") failed to load!"
            else:
                print("Start output selection of 2nd layer hexagons...")
                
                bufferLayer = QgsVectorLayer(regionShapefile, shapeTitle, "ogr")
                if not bufferLayer.isValid():
                    print "Layer (" + regionShapefile + ") failed to load!"
                else:
                    writeCoveredHexagons(shp_hex1_real, regionShapefile, targetLayerPath1)
                    print("Output selection 1st Hexagon file " + targetLayerPath1)
                    writeCoveredHexagons(shp_hex2_real, regionShapefile, targetLayerPath2)
                    print("Output selection 2nd Hexagon file " + targetLayerPath2)
                    
    if deleteRawFiles:
        print("Delete buffered and raw files from " + outdir)
        
        for fl in glob.glob(shp_hex1_real[0:-4] + ".*"):
            os.remove(fl)
        for fl in glob.glob(shp_hex2_real[0:-4] + ".*"):
            os.remove(fl)
        
        
QgsApplication.exitQgis()