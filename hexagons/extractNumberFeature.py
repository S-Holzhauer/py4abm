'''

Lists number of agents (given), hexagon width, and 
number of hexagon features of shapefiles in the given inputDir

Created on 23 Jul 2014

@author: Sascha Holzhauer

'''

import os
import glob

import csv

import qgis.core
import re


inputDir    = "C:\Data\Dis\workspace\NetworkGenerator\config\shapes\hexagons"
outputFileName = "C:\Data\Dis\workspace\NetworkGenerator\config\shapes\hexagons/HexagonNumbers.csv"
qgisPath = "C:/Program Files/OSGeo4W64/apps/qgis"


# Switches
doGridCreation = 1
doGridDevitionCreation = 0
doBuffering = 0
doHexagonShapefile = 1
doSelectHexagons = 1
doClean = 1

debug = 0


# supply path to where is your qgis installed
qgis.core.QgsApplication.setPrefixPath(qgisPath, True)

# load providers
qgis.core.QgsApplication.initQgis()

populations = {'4800':1, '10000':5, '25000':25, '50000':125}

fp = open(outputFileName, 'wb')
a = csv.writer(fp, delimiter=',')
a.writerow(['AgentNumber', 'HexagonWidth', 'HexagonNumber'])

for run in os.listdir(inputDir):
    for pathAndFilename in glob.iglob(os.path.join(inputDir, run, "*.shp")):
        numbers = re.findall(r'[0-9]+\.*[0-9]+', run)
 
        if len(numbers) > 1:
            layer = qgis.core.QgsVectorLayer(pathAndFilename, "NN", "ogr")
            numHexs = layer.dataProvider().featureCount()
            
            print(pathAndFilename + ": " +  str(numHexs))
            
            print(numbers)
            
            # parse area
            area = numbers[0]
            
            # parse hexwidth
            hexwidth = numbers[1]
            
            a.writerow([populations[area], hexwidth, numHexs])
        

qgis.core.QgsApplication.exitQgis()

# export to csv