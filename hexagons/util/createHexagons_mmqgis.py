'''
Created on 18 Sep 2014

@author: Sascha Holzhauer

Adapted from mmqgisx_grid_algorithm#processAlgorithm in
qgis\python\plugins\processing\algs\mmqgisx\MMQGISXAlgorithms.py

'''

from PyQt4.QtCore import QVariant, QFile
from qgis.core import QgsField, QgsFields, QgsVectorFileWriter, QgsFeature, QgsGeometry, QgsPoint, QGis, QgsCoordinateReferenceSystem
import math


def mmqgis_grid(savename, hspacing, vspacing, width, height, originx, originy, crs_string="EPSG:7416"):
    vspacing = float(vspacing)
    hspacing = float(hspacing)
    
    if len(savename) <= 0:
        return "No output filename given"

    if (hspacing <= 0) or (vspacing <= 0):
        return "Invalid grid spacing: " + unicode(hspacing) + " / " + unicode(vspacing)
    
    if (width <= hspacing) or (width < vspacing):
        return "Invalid width / height: " + unicode(width) + " / " + unicode(height)

    fields = QgsFields()
    fields.append(QgsField("left", QVariant.Double, "real", 24, 16, "left"))
    fields.append(QgsField("top", QVariant.Double, "real", 24, 16, "top"))
    fields.append(QgsField("right", QVariant.Double, "real", 24, 16, "right"))
    fields.append(QgsField("bottom", QVariant.Double, "real", 24, 16, "bottom"))

    if QFile(savename).exists():
        if not QgsVectorFileWriter.deleteShapeFile(savename):
            print("Failure deleting existing shapefile: " + savename)
            return "Failure deleting existing shapefile: " + savename

    shapetype = QGis.WKBPolygon
    
    # print gridtype + "," + str(shapetype)
    crs = QgsCoordinateReferenceSystem()
    crs.createFromString(crs_string)
    
    outfile = QgsVectorFileWriter(savename, "utf-8", fields, shapetype, crs);

    if (outfile.hasError() != QgsVectorFileWriter.NoError):
        return "Failure creating output shapefile: " + unicode(outfile.errorMessage())
    
    # To preserve symmetry, hspacing is fixed relative to vspacing
    xvertexlo = 0.288675134594813 * vspacing;
    xvertexhi = 0.577350269189626 * vspacing;
    hspacing = xvertexlo + xvertexhi

    columns = int(math.ceil(float(width) / hspacing))
    rows = int(math.ceil(float(height) / vspacing))

    for column in range(0, columns):
        # (column + 1) and (row + 1) calculation is used to maintain 
        # topology between adjacent shapes and avoid overlaps/holes 
        # due to rounding errors

        x1 = originx + (column * hspacing)        # far left
        x2 = x1 + (xvertexhi - xvertexlo)        # left
        x3 = originx + ((column + 1) * hspacing)    # right
        x4 = x3 + (xvertexhi - xvertexlo)        # far right

        for row in range(0, rows):
            if (column % 2) == 0:
                y1 = originy + (((row * 2) + 0) * (vspacing / 2.0))    # hi
                y2 = originy + (((row * 2) + 1) * (vspacing / 2.0))    # mid
                y3 = originy + (((row * 2) + 2) * (vspacing / 2.0))    # lo
            else:
                y1 = originy + (((row * 2) + 1) * (vspacing / 2.0))    # hi
                y2 = originy + (((row * 2) + 2) * (vspacing / 2.0))    # mid
                y3 = originy + (((row * 2) + 3) * (vspacing / 2.0))    #lo

            polyline = []
            polyline.append(QgsPoint(x1, y2))
            polyline.append(QgsPoint(x2, y1))
            polyline.append(QgsPoint(x3, y1))
            polyline.append(QgsPoint(x4, y2))
            polyline.append(QgsPoint(x3, y3))
            polyline.append(QgsPoint(x2, y3))
            polyline.append(QgsPoint(x1, y2))

            feature = QgsFeature()
            feature.setGeometry(QgsGeometry.fromPolygon([polyline]))
            # feature.setAttributes([ QVariant(x1), QVariant(y1), QVariant(x4), QVariant(y3) ])
            feature.setAttributes([ x1, y1, x4, y3 ])
            outfile.addFeature(feature)

    del outfile

    return None
