'''
Created on 24 Jul 2014

Filter those hexagon polygons which are not overlapping with the region area.

@author: Sascha Holzhauer
'''

from qgis.core import QgsVectorLayer, QgsVectorFileWriter, QgsFeature, QgsGeometry, QgsFeatureRequest
import ftools_utils

def writeCoveredHexagons(layerToSelect, maskLayer, targetLayerPath):
    print("Select features from " + layerToSelect + " that intersect with " + maskLayer)
    inputLayer = QgsVectorLayer(layerToSelect, "Layer to select", "ogr")
    if not inputLayer.isValid():
        print "Layer failed to load!"
     
    selectLayer = QgsVectorLayer(maskLayer, "Mask layer", "ogr")    
    if not selectLayer.isValid():
        print "Layer failed to load!"  
    
    inputProvider = inputLayer.dataProvider()
    selectProvider = selectLayer.dataProvider()
    
    feat = QgsFeature()
    infeat = QgsFeature()
    geom = QgsGeometry()
    selectedSet = set()
    index = ftools_utils.createIndex(inputProvider)

    selectFit = selectProvider.getFeatures()
    
    print("Process features...")
    while selectFit.nextFeature(feat):
        geom = QgsGeometry(feat.geometry())
        intersects = index.intersects(geom.boundingBox())
        for fid in intersects:
            inputProvider.getFeatures( QgsFeatureRequest().setFilterFid( int(fid) ) ).nextFeature( infeat )
            tmpGeom = QgsGeometry( infeat.geometry() )
            if geom.intersects(tmpGeom): # & infeat.id() not in selectedSet:
                selectedSet.add(infeat.id())
    
    #  newlayer = QgsVectorLayer(inputLayer.source(), inputLayer.name(), inputLayer.providerType())
    #  provider = newlayer.dataProvider()
    #  features = provider.getFeatures()
    #  featu = QgsFeature()
    #  while features.nextFeature(featu):
    #      newlayer.deleteFeature(featu.id())
    # print("Feature count: " + str(newlayer.featureCount()))
    # ftools_utils.writeVectorLayerToShape(newlayer, targetLayerPath, "CP1250")
    
    outfile = QgsVectorFileWriter(targetLayerPath, "utf-8", inputLayer.dataProvider().fields(),
            inputLayer.dataProvider().geometryType(), inputLayer.crs())
    
    for index, feature in enumerate(inputLayer.dataProvider().getFeatures()):
        if (index in selectedSet):
            outfile.addFeature(feature)
    del outfile
    return None