'''
Created on 18 Sep 2014

@author: Sascha Holzhauer

Adapted from ftools/tools/doVectorGrid.py

'''
from PyQt4.QtCore import QVariant, QFile
from qgis.core import QgsField, QgsFields, QgsVectorFileWriter, QgsFeature, QgsGeometry, QgsPoint, QGis


def createGridPolygons(shapefileName, crs, xMinimum, xMaximum, yMinimum, yMaximum, xOffset, yOffset):

    fields = QgsFields()
    fields.append( QgsField("ID", QVariant.Int) )
    
    fields.append( QgsField("XMIN", QVariant.Double) )
    fields.append( QgsField("XMAX", QVariant.Double) )
    fields.append( QgsField("YMIN", QVariant.Double) )
    fields.append( QgsField("YMAX", QVariant.Double) )
    fieldCount = 5
    
    check = QFile(shapefileName)
    if check.exists():
        if not QgsVectorFileWriter.deleteShapeFile(shapefileName):
            return
    writer = QgsVectorFileWriter(shapefileName, "utf-8", fields, QGis.WKBPolygon, crs)

    outFeat = QgsFeature()
    outFeat.initAttributes(fieldCount)
    outFeat.setFields(fields)
    outGeom = QgsGeometry()
    idVar = 0
        
    y = yMaximum
    while y >= yMinimum:
        x = xMinimum
        while x <= xMaximum:
            pt1 = QgsPoint(x, y)
            pt2 = QgsPoint(x + xOffset, y)
            pt3 = QgsPoint(x + xOffset, y - yOffset)
            pt4 = QgsPoint(x, y - yOffset)
            pt5 = QgsPoint(x, y)
            polygon = [[pt1, pt2, pt3, pt4, pt5]]
            outFeat.setGeometry(outGeom.fromPolygon(polygon))
            outFeat.setAttribute(0, idVar)
            outFeat.setAttribute(1, x)
            outFeat.setAttribute(2, x + xOffset)
            outFeat.setAttribute(3, y - yOffset)
            outFeat.setAttribute(4, y)
            writer.addFeature(outFeat)
            idVar = idVar + 1
            x = x + xOffset
        y = y - yOffset
        
    del writer