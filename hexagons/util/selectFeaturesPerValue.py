'''
Created on 24 Jul 2014

Filter those hexagon polygons which are not overlapping with the region area.

@author: Sascha Holzhauer
'''

from qgis.core import QgsVectorLayer, QgsVectorFileWriter, QgsFeature, QgsGeometry, QgsFeatureRequest
import ftools_utils

def writeFeaturesPerValue(layerToSelect, value, varName, targetLayerPath):
    print("Select features from " + layerToSelect + " with value " + value + " for " + varName)
    
    inputLayer = QgsVectorLayer(layerToSelect, "Layer to select", "ogr")
    if not inputLayer.isValid():
        print "Layer failed to load!"
     
    inputProvider = inputLayer.dataProvider()
    
    feat = QgsFeature()
    infeat = QgsFeature()
    geom = QgsGeometry()
    selectedSet = set()
    index = ftools_utils.createIndex(inputProvider)

    features = inputProvider.getFeatures()
    
    print("Process features...")
    while features.nextFeature(feat):
        
        # TODO not further implemented from here!!!
        
        geom = QgsGeometry(feat.geometry())
        intersects = index.intersects(geom.boundingBox())
        for fid in intersects:
            inputProvider.getFeatures( QgsFeatureRequest().setFilterFid( int(fid) ) ).nextFeature( infeat )
            tmpGeom = QgsGeometry( infeat.geometry() )
            if geom.intersects(tmpGeom): # & infeat.id() not in selectedSet:
                selectedSet.add(infeat.id())
    
    #  newlayer = QgsVectorLayer(inputLayer.source(), inputLayer.name(), inputLayer.providerType())
    #  provider = newlayer.dataProvider()
    #  features = provider.getFeatures()
    #  featu = QgsFeature()
    #  while features.nextFeature(featu):
    #      newlayer.deleteFeature(featu.id())
    # print("Feature count: " + str(newlayer.featureCount()))
    # ftools_utils.writeVectorLayerToShape(newlayer, targetLayerPath, "CP1250")
    
    outfile = QgsVectorFileWriter(targetLayerPath, "utf-8", inputLayer.dataProvider().fields(),
            inputLayer.dataProvider().geometryType(), inputLayer.crs())
    
    for index, feature in enumerate(inputLayer.dataProvider().getFeatures()):
        if (index in selectedSet):
            outfile.addFeature(feature)
    del outfile
    return None