'''
Created on 13 Aug 2014

Import Thunderbird ImportExportTools to copy multiple messages to clipboard
(https://addons.mozilla.org/en-US/thunderbird/addon/importexporttools/)

Not suitable for aborted jobs!

@author: Sascha Holzhauer
'''

import csv
import re
import glob, os

importDir = "C:/Users/sholzhau/Desktop/Temp/EddieMessages"
csvFileName = "C:/Data/LURG/Crafty/Documents/CRAFTY_EddieJobs.csv"


csvFile = open(csvFileName, 'ab')
writer = csv.writer(csvFile)  

for pathAndFilename in glob.iglob(os.path.join(importDir, '*.txt')):
    datalist = []
    inputFile = file(pathAndFilename, "r")
    for i in range(1, 6):
        line =  inputFile.readline()
    while line != "":
        if "Job" in line:
            datalist.append(re.findall("(\d+)", line)[0])
            datalist.append(line[line.find("(")+1:line.find(")")])
            print("Storing job " + line[line.find("(")+1:line.find(")")])
        elif "=" in line:
            content = line.split("=")
            datalist.append(content[1].strip())
        line =  inputFile.readline()
    writer.writerow((datalist))
    
    inputFile.close()
    os.remove(pathAndFilename)
    
csvFile.close()
print("...finished")
