'''
Created on 20 April 2015

Extract marginal utilities from a CRAFTY log file

TODO use OptionParser

@author: Sascha Holzhauer
'''

import csv
import re
import Tkinter as tk
import tkFileDialog
from listboxdialog import ListBoxChoice

root = tk.Tk()


list = []
list.append("Marginal utilities (regional)")
list.append("Marginal Utilities")
list.append("World per-cell residual")
list.append("World Demand")

indicator = ListBoxChoice(root, "Indicator","Choose indicator", list).returnValue()

root.withdraw()

options = {}
options['filetypes'] = [('all files', '.*'), ('text files', '.txt')]
options['initialdir'] = 'L:\Projects\Impressions\Modelling'
options['parent'] = root
options['title'] = 'Specify log file!'
importFile = tkFileDialog.askopenfilename(**options)


csvFilename = indicator.replace(" ", "_").replace("(", "").replace(")", "")

options = {}
options['filetypes'] = [('CSV', '.csv')]
options['defaultextension'] = '.csv'
options['initialdir'] = 'L:\Projects\Impressions\Modelling'
options['initialfile'] = csvFilename
options['parent'] = root
options['title'] = 'Specify target CSV file!'
csvFile = tkFileDialog.asksaveasfile(mode='wb', **options)

startyear = 2010
datalist = ["Year", "Meat", "Cereal", "Recreation", "Timber"]

writer = csv.writer(csvFile)  

foundline = False
inputFile = file(importFile, "r")
line =  inputFile.readline()

writer.writerow((datalist))
countervar = 0
while line != "":
    countervar = countervar + 1
    if indicator in line:
        datalist = []
        datalist.append(startyear)
        # ([-\d]+\.\d*)
        # [-+]?(\d+(\.\d*)?|\.\d+)([eE][-+]?\d+)?
        for utility in re.findall("([-\d]+\.\d*[eE]?[-+]?\d*)", line):
            datalist.append(str(float(utility)))
        writer.writerow((datalist))
        startyear = startyear + 1
        foundline = True
    line =  inputFile.readline()

if not foundline:
    print("No line found for indicator " + indicator + "!")
    
print("Number of lines parsed: " + str(countervar))
inputFile.close()    
csvFile.close()
print("...finished")
