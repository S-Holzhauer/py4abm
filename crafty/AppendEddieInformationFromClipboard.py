'''
Created on 13 Aug 2014

Import Thunderbird ImportExportTools to copy multiple messages to clipboard
(https://addons.mozilla.org/en-US/thunderbird/addon/importexporttools/)

@author: Sascha Holzhauer
'''

import win32clipboard as w 
import csv
import re

csvFileName = "C:/Data/LURG/Crafty/Documents/CRAFTY_EddieJobs.csv"

def getText(): 
    w.OpenClipboard() 
    d=w.GetClipboardData(w.CF_TEXT) 
    w.CloseClipboard() 
    return d 

csvFile = open(csvFileName, 'ab')
writer = csv.writer(csvFile)  

text = getText().split("\r\n")[2:-2]
datalist = []
for line in text:
    if "Job" in line:
        datalist.append(re.findall("(\d+)", line)[0])
        datalist.append(line[line.find("(")+1:line.find(")")])
        print("Storing job " + line[line.find("(")+1:line.find(")")])
    elif "=" in line:
        content = line.split("=")
        datalist.append(content[1].strip())

writer.writerow((datalist))
csvFile.close()
print("...finished")
