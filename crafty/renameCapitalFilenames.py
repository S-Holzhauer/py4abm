'''
Created on 30 Jul 2014

@author: Sascha Holzhauer

Rename capital files. Replaces the number with country codes taken from the given table csv file
and adds regionalisation.

Instructions:
1. Adjust parameter variables:
    a) projectdir
    b) setting folder name
    c) world
    d) regionalisation
    e) Make sure the path to the country code file is correct
    
2. Run this python script


TODO parameters via command line options

'''
import glob, re

import os
import csv

projectdir      = "../../"
setting         = "setA"
world           = "EU28"
regionalisation = 26


indir           = projectdir + "/data/" + setting + "/worlds/" + world + "/regionalisations/" \
                  "" + str(regionalisation) + "capitals"
 
countryCodeFile = projectdir + "/worlds/" + world + "/regionalisations/" \
                  "" + str(regionalisation) + "/CountryCodeNumberMapping.csv"


def rename(singledir, pattern, replace, substitution):
    for pathAndFilename in glob.iglob(os.path.join(singledir, pattern)):
        title, ext = os.path.splitext(os.path.basename(pathAndFilename))
        
        
        if re.findall(r'\d+', title):
            replacement = re.sub(r'\d+', codes[re.findall(r'\d+', title)[0]], title) + "_Capitals"
        else:
            replacement = title
        replacement = replacement.replace(replace, substitution)
        print('Rename ' + title +  ext + ' to ' + replacement + ext)
        os.rename(pathAndFilename, 
                 os.path.join(singledir, replacement + ext))
        
        
# get country codes:
reader = csv.DictReader(open(countryCodeFile))
codes = dict()
for row in reader:
    if not row["Code"] == "":
        codes[row['Number']] = row["Code"]
            

rename(indir, r'*.csv', r'Country', str(regionalisation) + "_Capitals")

