'''
Created on 04 Feb 2015

@author: Sascha Holzhauer

Reads a template XML file and substitutes the following place holders:
- %VERSION%
- %WORLD%
- %REGIONALISATION%
- %REGION%
- %HEXWIDTH1%
- %HEXWIDTH2%

Set parameters!

'''

import os, shutil
import csv


projectdir          = "../../../"
version             = "_version"
world               = "EU28"
regionalisations    = [26]
randomseed          = 0

workingDir          = projectdir + "/data/" + version

hexwidth1           = "75"
hexwidth2           = "10"

templateSnHDFF      = workingDir + "/socialNetwork/templates/SocialNetwork_HDFF.xml"
templateSnRestore   = workingDir + "/socialNetwork/templates/SocialNetwork_Restore.xml"


print("Build SocialNetwork XML files for scenario file ")

for regionalisation in regionalisations:
    print("Handle regionalisation " + str(regionalisation) + "...")
    
    countryCodeFile = projectdir + "/data/" + version + "/worlds/" + world + "/regionalisations/" \
                  "" + str(regionalisation) + "/CountryCodeNumberMapping.csv"

    outputFolderHdff = workingDir + '/worlds/' + world + '/regionalisations/' + str(regionalisation) + '/socialNetworks/'
                    
    outputFolderRestore = workingDir + '/worlds/' + world + '/regionalisations/' + str(regionalisation) + '/socialNetworks/'\
                    "" + str(randomseed) + "/"
    
    print("Check output directory " + outputFolderHdff + "...")
    if os.path.exists(outputFolderHdff):
        shutil.rmtree(outputFolderHdff, ignore_errors=True)    
    os.makedirs(outputFolderHdff)

    print("Check output directory " + outputFolderRestore + "...")
    if os.path.exists(outputFolderRestore):
        shutil.rmtree(outputFolderRestore, ignore_errors=True)    
    os.makedirs(outputFolderRestore)
        
    # get country codes:
    reader = csv.DictReader(open(countryCodeFile))
    codes = []
    for row in reader:
        if not row["Code"] == "":
            codes.append(row["Code"])
    
    for region in codes:
        print("Handle region " + region + "...")
        
        templateSnHdffFile = open(templateSnHDFF, "r") 
        outFileHdff = open(outputFolderHdff +  str(regionalisation) + '_' + region + '_' + "SocialNetwork_HDFF.xml", 'w')
        
        inputLine =  templateSnHdffFile.readline()
        while inputLine != "":
            inputLine = inputLine.replace("%VERSION%", version)
            inputLine = inputLine.replace("%WORLD%", world)
            inputLine = inputLine.replace("%REGIONALISATION%", str(regionalisation))
            inputLine = inputLine.replace("%REGION%", region)
            inputLine = inputLine.replace("%HEXWIDTH1%", hexwidth1)
            inputLine = inputLine.replace("%HEXWIDTH2%", hexwidth2)
            outFileHdff.write(inputLine)
            inputLine =  templateSnHdffFile.readline()
    
        templateSnHdffFile.close()
        outFileHdff.close()

        templateSnRestoreFile = open(templateSnRestore, "r") 
        outFileRestore = open(outputFolderRestore +  str(regionalisation) + '_' + region + '_' + "SocialNetwork_Restore.xml", 'w')
        
        inputLine =  templateSnRestoreFile.readline()
        while inputLine != "":
            inputLine = inputLine.replace("%VERSION%", version)
            inputLine = inputLine.replace("%WORLD%", world)
            inputLine = inputLine.replace("%REGIONALISATION%", str(regionalisation))
            inputLine = inputLine.replace("%REGION%", region)
            inputLine = inputLine.replace("%HEXWIDTH1%", hexwidth1)
            inputLine = inputLine.replace("%HEXWIDTH2%", hexwidth2)
            inputLine = inputLine.replace("%RANDOMSEED%", str(randomseed))
            outFileRestore.write(inputLine)
            inputLine =  templateSnRestoreFile.readline()
        templateSnRestoreFile.close()
        outFileRestore.close()
        
print("Done!")