'''
Created on 30 Jul 2014

@author: Sascha Holzhauer

Rename demand files. Replaces the number with country codes taken from the given table csv file
and adds regionalisation.

Pattern to achieve: 26_A1_demands_AT.csv

Instructions:
1. Adjust parameter variables:
    a) projectdir
    b) setting folder name
    c) world
    d) regionalisation
    e) Make sure the path to the country code file is correct
    
2. Run this python script


TODO parameters via command line options

'''
import glob, re

import os
import csv

projectdir      = "../../../"
setting         = "setA"
world           = "EU28"
scenarios       = {"A1", "A2", "B1", "B2"}

regionalisation = 26


indir           = projectdir + "/data/" + setting + "/worlds/" + world + "/regionalisations/" \
                  "" + str(regionalisation) + "/"
 
countryCodeFile = projectdir + "/data/" + setting + "/worlds/" + world + "/regionalisations/" \
                  "" + str(regionalisation) + "/CountryCodeNumberMapping.csv"


def rename(singledir, pattern, replace, substitution):
    print("Look for files in " + singledir + "...")
    for pathAndFilename in glob.iglob(os.path.join(singledir, pattern)):
        title, ext = os.path.splitext(os.path.basename(pathAndFilename))
        
        replacement = title.replace(replace, "XXX")
        if len(re.findall(r'\d+', replacement)) > 0:
            replacement = re.sub(r'ctry\d+', codes[re.findall(r'\d+', replacement)[0]], replacement)
        replacement = replacement.replace("XXX", substitution)
        print('Rename ' + title +  ext + ' to ' + replacement + ext)
        os.rename(pathAndFilename, 
                 os.path.join(singledir, replacement + ext))
        
# get country codes:
reader = csv.DictReader(open(countryCodeFile))
codes = dict()
for row in reader:
    if not row["Code"] == "":
        codes[row['Number']] = row["Code"]
            

for scenario in scenarios:
    rename(indir + scenario, r'*.csv', scenario, str(regionalisation) + "_" + scenario + "_")
