The Py4ABM project contains some useful python scripts for the setup and application of agent-based models (ABM).
The features include (version 0.2.2):

- Generation of hexagon shapefiles for HDFF network generation:
	- createHexagons_MultiSquareRegion.py > Uses world width and regionalisation pattern to determine area to cover by hexagons  
										  (no raster data as input required).
	- createHexagons_SingleRegion.py > Uses raster data as input to determine area to cover by hexagons.

	- createHexagons_RegionFromShapefileDir.py
	
	- createHexagons_RegionsFromShapefileDirBuffered.py
	
- Create Grid Engine scripts to run CRAFTY on a linux cluster:
	craftybatch.py
	 
- Extract marginal utilities from a CRAFTY log file:
	extractMarginalUtilityFromFiles.py
	
- List number of agents (given), hexagon width, and number of hexagon features of shapefiles in the given inputDir:
	extractNumberFeature.py

- create Qsub script for R processes

- create social network XML files

- Rename capital filenames

- Rename demand filenames

- Append Eddie information from email textfiles to CSV file

Creation of hexagons requires QGIS, python plugin mmqgis for QGIS, and python (usually within QGis) to be installed:
- QGIS: http://www.qgis.org/
- mmqgis: http://michaelminn.com/linux/mmqgis/

Add these folders to your python path in case you're using the OSGeo distribution of QGIS:

- <OSGeo-Home>\apps\Python27
- <OSGeo-Home>\apps\Python27\Lib (not required for heagon creation)
- <OSGeo-Home>\apps\Python27\Lib\site-packages
- <OSGeo-Home>\apps\qgis\python
- <OSGeo-Home>\apps\qgis\python\plugins (not required for heagon creation)
- <OSGeo-Home>\apps\qgis\python\plugins\fTools\tools


Additionally, these need be in your PATH environment variable (Windows):

- <OSGeo-Home>\bin;
- <OSGeo-Home>\apps\qgis;
- <OSGeo-Home>\apps\qgis\bin;

When executing from another director than in the project, add:
- <Path to Py4ABM>/hexagons
